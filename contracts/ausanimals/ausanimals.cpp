#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/asset.hpp>
#include <cstdlib>

using namespace eosio;

class ausanimals : public eosio::contract {
 public:
    ausanimals(account_name self)
      :eosio::contract(self),
      sightings(_self, _self)
      {}

      struct issue {
           account_name to;
                     eosio::asset quantity;
                     std::vector<std::string> uri;
		            std::string name;
                     std::string memo;
      };

	// @abi table sigtable i64
    struct sighting {
      uint64_t id;
      std::string uri;
      std::string type;

      uint64_t primary_key()const { return id; }
      std::string by_type()const { return type; }
    };

    typedef eosio::multi_index<N(sigtable), sighting> sigtable;
    sigtable sightings;

     /// @abi action
     void add( account_name to, uint64_t photo_hash, std::string type ) {

            std::string color, size, rareity;

            // Clearly would do a better job of this IRL
            if(photo_hash >= 1000000000 && photo_hash < 5000000000) { 
                color = "Red";
                size = "Small";
                rareity = "Common";
            }
                             
            if(photo_hash >= 3000000000 && photo_hash < 8000000000) {
                color = "Green";
                size = "Medium";
                rareity = "Rare";
            }

            if(photo_hash >= 8000000000 && photo_hash <= 9999999999) {
                color = "Gold";
                size = "Large";
                rareity = "Super Rare";
            }

            std::string uri = std::to_string(photo_hash) + "_" + type + "_" + color + "_" + size + "_" + rareity;

             print( "URI: ", uri, "hello" );

            eosio::asset asset{1, S(0,JARMBI)};

            sightings.emplace(_self, [&](auto& new_sighting) {
                new_sighting.uri  = uri;
                new_sighting.type = type;
                new_sighting.id = photo_hash;
            });
            
            issue iss = {
                    to, asset, {uri}, type, ""};

            eosio::action(
                std::vector<eosio::permission_level>(1,{_self, N(active)}),
                N(eosi.nft), N(issue), iss
            ).send();

     } 
};

EOSIO_ABI( ausanimals, (add) )