import 'bootstrap/dist/css/bootstrap.min.css';


import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import * as firebase from "firebase";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCtz58AQQTXxEjNfR7sG96Sys3_UPWUKpc",
  authDomain: "ausanimals-e0aa5.firebaseapp.com",
  databaseURL: "https://ausanimals-e0aa5.firebaseio.com",
  projectId: "ausanimals-e0aa5",
  storageBucket: "ausanimals-e0aa5.appspot.com",
  messagingSenderId: "45604755626"
};
firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
