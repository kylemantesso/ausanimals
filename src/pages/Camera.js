import React from 'react';
import { Page } from '../components/Page';
import { getVision } from '../service/VisionService';
import { upload } from '../service/StorageService';
import {Camera} from "../components/Camera";
import {Button, Fade, ListGroup, ListGroupItem} from 'reactstrap';
import { Progress } from 'reactstrap';

import styled from 'styled-components';

const ResponsiveImage = styled.img`
  max-width: 100%;
    height: auto;
`

const Para = styled.p`
  font-size: 18px;
`

const LANDSCAPES = ['Beach', 'Ocean', 'Sky', 'Sand', 'Sea', 'Bondi Beach'];

function getRandom(min, max) {
  return Math.round(Math.random() * (max - min + 1) + min);
}

const style = {
  preview: {
    position: 'relative',
  },
  captureContainer: {
    display: 'flex',
    position: 'absolute',
    justifyContent: 'center',
    zIndex: 1,
    bottom: 0,
    width: '100%',
  },
  captureButton: {
    backgroundColor: '#fff',
    borderRadius: '50%',
    height: 56,
    width: 56,
    color: '#000',
    margin: 20,
  },
  captureImage: {
    width: '100%',
  },
};

export class CameraPage extends React.Component {
  constructor(props) {
    super(props);
    this.takePicture = this.takePicture.bind(this);
    this.state = { isLoading: false, name: '', webEntities: '', bestGuess: '', captured: false, loadedAnimal: false, loadingAnimal: false };
  }


  async getToken(isAnimal) {

    this.setState({loadingAnimal: true});

    const trans = await fetch('https://us-central1-ausanimals-e0aa5.cloudfunctions.net/getTokenDetails?token='+this.state.token+'&account=john&type='+this.state.webEntities).then(res => res.json());


    console.log('trans',trans);


    //const res = await eosApi.getTableRows(true, 'ausanimals', 'ausanimals', 'sighting_table', 'id', 0, 0, 100);

    if(isAnimal) {
      const animal = await fetch('https://us-central1-ausanimals-e0aa5.cloudfunctions.net/getAnimalDetails?token=' + this.state.token).then(res => res.json());
      console.log('animal', animal);

      const [id, type, color, size, rareity] = animal.uri.split("_");

      this.setState({color, size, rareity, loadedAnimal: true, loadingAnimal: false});
    } else {

    }

  }

  async takePicture() {

    this.setState({ isLoading: true, captured: true });

    this.camera.capture().then(blob => {
      this.setState({ imgUrl: URL.createObjectURL(blob), token: getRandom(1000000000, 9999999999) });

      upload(this.state.token, blob).then(async path => {
        const webDetection = await getVision(path);
        console.log('webDetection', JSON.stringify(webDetection));

        if(webDetection.responses[0].webDetection) {
          this.setState({
            isLoading: false,
            bestGuess: webDetection.responses[0].webDetection.bestGuessLabels[0].label,
            webEntities: webDetection.responses[0].webDetection.webEntities[0].description
          });

        } else {
          // google cloud is broken, this is a backup only
          this.setState({
            isLoading: false,
            webEntities: 'Koala'
          });
        }

        
        if( LANDSCAPES.indexOf(this.state.webEntities) > -1 ) {
          this.getToken(false);
        } else {
          this.getToken(true);
        }
      });

      this.img.src = URL.createObjectURL(blob);
      this.img.onload = () => {
        URL.revokeObjectURL(this.src);
      };
    });
  }

  render() {
    return (
      <Page>
        {!this.state.imgUrl && (
          <Camera
            style={style.preview}
            ref={cam => {
              this.camera = cam;
            }}
          >
            <div style={style.captureContainer} onClick={this.takePicture}>
              <Button color="success" size="lg">Catch Jarmbi!</Button>
            </div>
          </Camera>
        )}
        <img
          style={style.captureImage}
          ref={img => {
            this.img = img;
          }}
        />
        {this.state.captured &&
        <div style={{textAlign: 'center'}}>
          <br />
          <br />

          <h4 style={{textAlign: 'center'}}>You collected a...</h4>
          {this.state.isLoading &&<Progress animated color="success" value={100} />
          }
          {!this.state.isLoading && this.state.captured && !(LANDSCAPES.indexOf(this.state.webEntities) > -1) &&
          <div>

            <Fade in={!this.state.isLoading && this.state.captured} tag="div">
              <h1 style={{textAlign: 'center'}}><strong>{this.state.webEntities}</strong></h1>
              <h3 style={{textAlign: 'center'}}><strong>Jarmbi</strong></h3>


              {this.state.webEntities.toLowerCase().includes('kangaroo') && <ResponsiveImage src="img/kang.jpg" /> }
              {this.state.webEntities.toLowerCase().includes('koala') && <ResponsiveImage src="img/koala.jpg" /> }
              {!this.state.webEntities.toLowerCase().includes('kangaroo') && !this.state.webEntities.toLowerCase().includes('koala') && <ResponsiveImage src="img/something-else.jpg" /> }
            </Fade>

            <br />
            <br />


            {this.state.loadingAnimal && <Progress animated color="success" value={100} /> }
            { this.state.loadedAnimal &&
              <div>
                <Para>You found a <strong><span style={{color: this.state.color }}>{this.state.color}</span></strong> {this.state.webEntities}!</Para>
                <Para>Your {this.state.webEntities}'s size is <strong>{this.state.size}</strong></Para>
                <Para>Your {this.state.webEntities} is <strong>{this.state.rareity}</strong></Para>

                <hr />
                <p>Buy a gift for your <strong>Jarmbi</strong></p>
                <ListGroup>
                  <ListGroupItem style={{display: 'flex', flex: 1}}><span style={{    marginTop: 6 }}>Food - <strong>20 JARMBI</strong></span><span style={{display: 'flex', flex: 1}}></span><Button outline color="primary">Buy</Button></ListGroupItem>
                  <ListGroupItem style={{display: 'flex', flex: 1}}><span style={{    marginTop: 6 }}>Home - <strong>40 JARMBI</strong></span><span style={{display: 'flex', flex: 1}}></span><Button outline color="primary">Buy</Button></ListGroupItem>
                  <ListGroupItem style={{display: 'flex', flex: 1}}><span style={{    marginTop: 6 }}>Hat - <strong>10 JARMBI</strong></span><span style={{display: 'flex', flex: 1}}></span><Button outline color="primary">Buy</Button></ListGroupItem>
                  <ListGroupItem style={{display: 'flex', flex: 1}}><span style={{    marginTop: 6 }}>Toy - <strong>15 JARMBI</strong></span><span style={{display: 'flex', flex: 1}}></span><Button outline color="primary">Buy</Button></ListGroupItem>

                </ListGroup>




              </div>


            }



          </div>}




          {!this.state.isLoading && this.state.captured && LANDSCAPES.indexOf(this.state.webEntities) > -1 &&
            <div>
              <Fade in={!this.state.isLoading && this.state.captured} tag="div">
                <h1 style={{textAlign: 'center'}}><strong>{this.state.webEntities}</strong></h1>
                <h3 style={{textAlign: 'center'}}><strong>habitat</strong></h3>

                <br />

                <p style={{textAlign: 'center'}}>Your Jarmbi can now live in this new habitat!</p>
              </Fade>
            </div>

          }

        </div>

        }
      </Page>
    );
  }
}
