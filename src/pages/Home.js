import React from 'react';
import {Flex, Page} from '../components/Page';
import {Button} from 'reactstrap';

import styled from 'styled-components';
import {Link} from "react-router-dom";


const Title = styled.h1`
font-size: 48px;
`

const Subtitle = styled.h3`
font-size: 24px;
color: #82817d;
`


export class HomePage extends React.Component {


  render() {
    return(
      <Page>
        <div style={{flexDirection: 'row'}}>
          <img width="100" src="img/logo.png" />
          <h3>Jarmbi</h3>
        </div>
        <br />
        <br />

        <Title>Discover.</Title>
        <Title>Snap.</Title>
        <Title>Collect.</Title>
        <Subtitle>Explore the Australian outback</Subtitle>
        <br />
        <br />
        <br />
        <Link to="/catch">

        <Button color="success" size="lg">Lets go!</Button>
        </Link>
      </Page>
    )
  }

}