const API_KEY = 'AIzaSyBDWzS7wx9FQ4MdOzEpxxcFyNwl802k_zU';

export const getVision = async path => {

  console.log('path', path);

  const request = {
    requests: [
      {
        image: {
          source: {
            imageUri: path
          }
        },
        features: [
          {
            "type":"WEB_DETECTION",
            "maxResults":5
          },
        ],
      },
    ],
  };

  const result = await fetch(
    `https://vision.googleapis.com/v1/images:annotate?key=${API_KEY}`,
    {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      contentType: 'application/json',
      body: JSON.stringify(request),
    }
  )
    .then(response => {
      return response.json()

    }) // parses response to JSON
    .catch(error => console.error(`Fetch Error =\n`, error));

  console.log('result', result);

  return result;
};
