
import * as firebase from 'firebase';

export async function upload (token, blob) {

  // Create a root reference
  const storageRef = firebase.storage().ref();

  const imageRef = storageRef.child('images/' + token + '.jpg');

  const snap = await imageRef.put(blob);

  console.log(snap);

  await imageRef.put(blob);

  return await imageRef.getDownloadURL();

  // return `gs://${snap.metadata.bucket}/${snap.metadata.fullPath}`;

}