import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom";
import {CameraPage} from "./pages/Camera";
import {HomePage} from "./pages/Home";



class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
          <Route exact path="/" component={HomePage}/>
          <Route exact path="/catch" component={CameraPage}/>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
