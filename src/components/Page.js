import React from 'react';
import styled from 'styled-components';

export const Page = styled.div`
max-width: 960px;
flex: 1 1 100%;
margin: 0 auto;
padding: 24px;
`

export const Flex = styled.div`
display: flex;
flex: 1;
`