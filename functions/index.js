const functions = require('firebase-functions');
const _cors = require('cors');
// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

const Eos = require('eosjs');

const eos = Eos({
  httpEndpoint: 'http://35.189.34.146:8888',
  chainId: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f',
  keyProvider: [
    "5JcWU73CEyCtJakuheT3j26RNBYpuy2bSMG67Wm5XFT3gNeLb9i",
    "5KhFDdjRYz8ZKHgGy53TkaPotFQQSiUazjuSwP3jgdYaKbbuBHp"
  ],
});

const EosApi = require('eosjs-api'); // Or EosApi = require('./src')



const options = {
  httpEndpoint: 'http://35.189.34.146:8888',
  chainId: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f',

}

const eosApi = EosApi(options);


const optionscors = {
  origin: '*',
};
const cors = _cors;

exports.getAnimalDetails = functions.https.onRequest((request, response) => {

  cors(optionscors)(request, response, () => getAnimal(request, response));

});

exports.getTokenDetails = functions.https.onRequest((request, response) => {

  cors(optionscors)(request, response, () => getToken(request, response));

});


function getToken(request, response) {
  const {token, type, account} = request.query;

  eos.transaction('ausanimals', ausanimals => {
    ausanimals.add(account, token, type,   {authorization: account + '@active'}
    );

  }).then(trans => {
    response.json(trans);
  });
}

function getAnimal(request, response) {
  const {token} = request.query;

  eosApi.getTableRows({
    json: true,
    code: "ausanimals",
    scope: "ausanimals",
    table: "sigtable",
    limit: 200
  }).then(res => {
    console.log('res', res);
    console.log('token', token);

    const animal = res.rows.filter(animal => animal.id == token)[0];

    console.log('res', animal);

    response.json(animal);
  });



}


